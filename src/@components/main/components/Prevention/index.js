import "./prevention.scss";
import React from 'react';
export const Prevention = () => {
    return (
        <div className="prevention">
            <div className="prevention__title">
                <h3 className="title">
                    Prevention COVID-19
                </h3>
            </div>
            <div className="prevention__description">
            <h5>How to prevent the spread of COVID-19:</h5>
                <ul>
                    <li>Keep a safe distance from others (at least 1 meter), even if they are not showing symptoms.</li>
                    <li>Wear a mask in public, especially when indoors or when social distancing is not possible.</li>
                    <li>Choose open, airy spaces instead of closed spaces. Open windows if indoors.</li>
                    <li>Wash your hands often. Use soap and water or an alcohol-based hand sanitizer.</li>
                    <li>Get vaccinated when it's your turn. Follow local guidelines for vaccination.</li>
                    <li>When coughing or sneezing, cover your nose and mouth with a tissue or your elbow.</li>
                    <li>Stay home when you feel unwell.</li>
                </ul>
                <p>Seek medical attention if you have a fever, cough, and difficulty breathing. Be sure to call ahead of time so your healthcare provider can direct you to the right medical facility. This helps protect you and prevents the spread of viruses and other infectious diseases.</p>
                <p>A well-fitting mask can help prevent the virus from spreading from the person wearing the mask to others. However, just wearing a mask is not enough to protect you from COVID-19. You must combine wearing a mask with keeping your distance and washing your hands. Follow the recommendations of your local health authorities.</p>
            </div>
        </div>
    )
}